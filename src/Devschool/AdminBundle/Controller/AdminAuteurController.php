<?php // src/Devschool/AdminBundle/Controller/AdminGenreController.php

namespace Devschool\AdminBundle\Controller;

use Devschool\BiblioBundle\Entity\Auteur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Devschool\AdminBundle\Form\AuteurType;

/**
 * @Route("/admin/auteurs")
 */
class AdminAuteurController extends Controller
{
    /**
     * @Route("/ajout", name="admin_auteur_ajout")
     */
    public function addAction(Request $request)
    {
        $auteur = new Auteur(); //on crée un nouveau Genre vide
        $form = $this->createForm(AuteurType::class, $auteur); //on le lie à un formulaire de type GenreType

        $form->handleRequest($request); //on lie le formulaire à la requête HTTP

        if ($form->isSubmitted() && $form->isValid()) { //si le formulaire a bien été soumis et qu'il est valide
            $auteur = $form->getData(); //on crée un objet Genre avec les valeurs du formulaire soumis

            $em = $this->getDoctrine()->getManager(); //on récupère le gestionnaire d'entités de Doctrine

            $em->persist($auteur); //on s'en sert pour enregistrer le genre (mais pas encore dans la base de données)

            $em->flush(); //écriture en base de toutes les données persistées

            return $this->redirectToRoute('admin_auteur_liste'); //puis on redirige l'utilisateur vers la page des genres
        }

        return $this->render(
            'DevschoolAdminBundle:Auteur:form.html.twig',
            ['form' => $form->createView()]
        );
    }
    /**
     * @Route("/liste", name="admin_auteur_liste")
     */
    public function listAction()
    {
      $auteurs = $this->getDoctrine()->getRepository('DevschoolBiblioBundle:Auteur')->findAll();

        return $this->render(
            'DevschoolAdminBundle:Auteur:list.html.twig',
            ['auteurs' => $auteurs]
        );
    }


    /**
     * @Route("/modif/{id}", name="admin_auteur_modif", requirements={"id": "\d+"})
     */
    public function editAction(Request $request, $id)
    {
        //on récupère le bon Genre en fonction de l'id donnée dans l'URL
        $auteur = $this->getDoctrine()->getRepository('DevschoolBiblioBundle:Auteur')->find($id);

        $form = $this->createForm(AuteurType::class, $auteur); //on le lie à un formulaire de type GenreType
        //Le formulaire sera donc prérempli avec les données de l'objet Genre récupéré en base de données.

        //puis on exécute le même traitement que pour l'ajout
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $auteur = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($auteur);
            $em->flush();

            return $this->redirectToRoute('admin_auteur_liste');
        }

        return $this->render(
            'DevschoolAdminBundle:Auteur:form.html.twig',
            ['form' => $form->createView()]
        );
    }



    /**
     * @Route("/supprimer/{id}", name="admin_auteur_supprimer", requirements={"id": "\d+"})
     */
    public function deleteAction($id)
    {
        //on récupère le bon Genre en fonction de l'id donnée dans l'URL
        $auteur = $this->getDoctrine()->getRepository('DevschoolBiblioBundle:Auteur')->find($id);

        $em = $this->getDoctrine()->getManager(); //on récupère le gestionnaire
        $em->remove($auteur); //on supprime cette entité
        $em->flush(); //exécution en base

        return $this->redirectToRoute('admin_auteur_liste'); //redirection vers la liste
    }
}
