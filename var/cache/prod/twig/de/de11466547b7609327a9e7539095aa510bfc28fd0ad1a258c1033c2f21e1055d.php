<?php

/* DevschoolBiblioBundle:Default:index.html.twig */
class __TwigTemplate_5bb6c96e2a81a77b6e66ce0f757ad6ab95ca427862fffa8200baa0f89cb00f15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("DevschoolBiblioBundle::layout.html.twig", "DevschoolBiblioBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DevschoolBiblioBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Accueil";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    <p>Bienvenue sur le site Devschool Symfony</p>
";
    }

    public function getTemplateName()
    {
        return "DevschoolBiblioBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "DevschoolBiblioBundle:Default:index.html.twig", "C:\\wamp64\\www\\devschool\\src\\Devschool\\BiblioBundle/Resources/views/Default/index.html.twig");
    }
}
