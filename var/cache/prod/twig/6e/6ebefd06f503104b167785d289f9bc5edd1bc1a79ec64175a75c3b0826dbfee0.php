<?php

/* DevschoolBiblioBundle:Livre:list.html.twig */
class __TwigTemplate_58857845651611390ff1e8be3aa250672ea14a664600b2f896ee59a510679374 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("DevschoolBiblioBundle::layout.html.twig", "DevschoolBiblioBundle:Livre:list.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DevschoolBiblioBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Liste des livres";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "    <h1>";
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, (isset($context["titre"]) ? $context["titre"] : null)), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["livres"]) ? $context["livres"] : null)), "html", null, true);
        echo ")</h1>

    <ul class=\"liste\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["livres"]) ? $context["livres"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["livre"]) {
            // line 11
            echo "            <li>
                <h2><a href=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("page_livre", array("id" => $this->getAttribute($context["livre"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["livre"], "titre", array()), "html", null, true);
            echo "</a></h2>
                ";
            // line 13
            if (($this->getAttribute($context["livre"], "auteur", array()) == "J. K. Rowling")) {
                // line 14
                echo "                    <strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["livre"], "auteur", array()), "html", null, true);
                echo "</strong>
                ";
            } else {
                // line 16
                echo "                    <span>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["livre"], "auteur", array()), "html", null, true);
                echo "</span>
                ";
            }
            // line 18
            echo "            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['livre'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "    </ul>
";
    }

    public function getTemplateName()
    {
        return "DevschoolBiblioBundle:Livre:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 20,  74 => 18,  68 => 16,  62 => 14,  60 => 13,  54 => 12,  51 => 11,  47 => 10,  38 => 7,  35 => 6,  29 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "DevschoolBiblioBundle:Livre:list.html.twig", "C:\\wamp64\\www\\devschool\\src\\Devschool\\BiblioBundle/Resources/views/Livre/list.html.twig");
    }
}
