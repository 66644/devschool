<?php

/* DevschoolAdminBundle::layout.html.twig */
class __TwigTemplate_69cb70d0e3174311af6020be65ed20cca4a18e5c2d16d2efac143d51902edada extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <style>
        body {margin:0;}

        ul.menu {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
            position: fixed;
            top: 0;
            width: 100%;
        }

        ul.menu li {
            float: left;
        }

        ul.menu li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        ul.menu li a:hover:not(.active) {
            background-color: #111;
        }

        .active {
            background-color: #4CAF50;
        }

        div.content { padding:20px; margin-top:30px; }
    </style>
    <title>Admin</title>
</head>
<body>
<link rel=\"stylesheet\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
<link href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "basepath", array()), "html", null, true);
        echo "/lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">
<link href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "basepath", array()), "html", null, true);
        echo "/css/styl.css\" rel=\"stylesheet\">
<link href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "basepath", array()), "html", null, true);
        echo "/css/styleboostrap.css\" rel=\"stylesheet\">
</head>
<body>
<div class=\"container\">
  <nav class=\"navbar navbar-default navbar-fixed-top \" role=\"navigation\">
      <div class=\"container\">
          <div class=\"navbar-header\">
  </div>
  <ul class=\"menu\">
    <li><a";
        // line 55
        if (twig_in_filter("livre", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo " href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_livre_liste");
        echo "\">Livres</a></li>
    <li><a";
        // line 56
        if (twig_in_filter("genre", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo " href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_genre_liste");
        echo "\">Genres</a></li>
      <li><a";
        // line 57
        if (twig_in_filter("auteur", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo " href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_auteur_liste");
        echo "\">Auteurs</a></li>
    <li><a";
        // line 58
        if (twig_in_filter("page_accueil", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo " href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("page_accueil");
        echo "\">Retour au site</a></li>
</ul>

<div class=\"content\">
    ";
        // line 62
        $this->displayBlock('body', $context, $blocks);
        // line 63
        echo "</div>

</body>
</html>
";
    }

    // line 62
    public function block_body($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "DevschoolAdminBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 62,  125 => 63,  123 => 62,  112 => 58,  104 => 57,  96 => 56,  88 => 55,  76 => 46,  72 => 45,  68 => 44,  64 => 43,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "DevschoolAdminBundle::layout.html.twig", "C:\\wamp64\\www\\devschool\\src\\Devschool\\AdminBundle/Resources/views/layout.html.twig");
    }
}
