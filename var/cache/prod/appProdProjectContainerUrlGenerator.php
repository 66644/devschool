<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdProjectContainerUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'admin_auteur_ajout' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminAuteurController::addAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/auteurs/ajout',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_auteur_liste' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminAuteurController::listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/auteurs/liste',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_auteur_modif' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminAuteurController::editAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/auteurs/modif',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_auteur_supprimer' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminAuteurController::deleteAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/auteurs/supprimer',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_genre_ajout' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminGenreController::addAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/genres/ajout',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_genre_liste' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminGenreController::listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/genres/liste',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_genre_modif' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminGenreController::editAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/genres/modif',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_genre_supprimer' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminGenreController::deleteAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/genres/supprimer',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_livre_ajout' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminLivreController::addAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/livres/ajout',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_livre_liste' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminLivreController::listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/livres/liste',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_livre_modif' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminLivreController::editAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/livres/modif',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'admin_livre_supprimer' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminLivreController::deleteAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/livres/supprimer',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'page_accueil' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Devschool\\BiblioBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'page_livres' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Devschool\\BiblioBundle\\Controller\\DefaultController::listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/livres',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'page_livre' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Devschool\\BiblioBundle\\Controller\\DefaultController::showAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/livre',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
