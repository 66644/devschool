<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/auteurs')) {
                // admin_auteur_ajout
                if ($pathinfo === '/admin/auteurs/ajout') {
                    return array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminAuteurController::addAction',  '_route' => 'admin_auteur_ajout',);
                }

                // admin_auteur_liste
                if ($pathinfo === '/admin/auteurs/liste') {
                    return array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminAuteurController::listAction',  '_route' => 'admin_auteur_liste',);
                }

                // admin_auteur_modif
                if (0 === strpos($pathinfo, '/admin/auteurs/modif') && preg_match('#^/admin/auteurs/modif/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_auteur_modif')), array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminAuteurController::editAction',));
                }

                // admin_auteur_supprimer
                if (0 === strpos($pathinfo, '/admin/auteurs/supprimer') && preg_match('#^/admin/auteurs/supprimer/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_auteur_supprimer')), array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminAuteurController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/genres')) {
                // admin_genre_ajout
                if ($pathinfo === '/admin/genres/ajout') {
                    return array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminGenreController::addAction',  '_route' => 'admin_genre_ajout',);
                }

                // admin_genre_liste
                if ($pathinfo === '/admin/genres/liste') {
                    return array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminGenreController::listAction',  '_route' => 'admin_genre_liste',);
                }

                // admin_genre_modif
                if (0 === strpos($pathinfo, '/admin/genres/modif') && preg_match('#^/admin/genres/modif/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_genre_modif')), array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminGenreController::editAction',));
                }

                // admin_genre_supprimer
                if (0 === strpos($pathinfo, '/admin/genres/supprimer') && preg_match('#^/admin/genres/supprimer/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_genre_supprimer')), array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminGenreController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/livres')) {
                // admin_livre_ajout
                if ($pathinfo === '/admin/livres/ajout') {
                    return array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminLivreController::addAction',  '_route' => 'admin_livre_ajout',);
                }

                // admin_livre_liste
                if ($pathinfo === '/admin/livres/liste') {
                    return array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminLivreController::listAction',  '_route' => 'admin_livre_liste',);
                }

                // admin_livre_modif
                if (0 === strpos($pathinfo, '/admin/livres/modif') && preg_match('#^/admin/livres/modif/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_livre_modif')), array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminLivreController::editAction',));
                }

                // admin_livre_supprimer
                if (0 === strpos($pathinfo, '/admin/livres/supprimer') && preg_match('#^/admin/livres/supprimer/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_livre_supprimer')), array (  '_controller' => 'Devschool\\AdminBundle\\Controller\\AdminLivreController::deleteAction',));
                }

            }

        }

        // page_accueil
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'page_accueil');
            }

            return array (  '_controller' => 'Devschool\\BiblioBundle\\Controller\\DefaultController::indexAction',  '_route' => 'page_accueil',);
        }

        if (0 === strpos($pathinfo, '/livre')) {
            // page_livres
            if ($pathinfo === '/livres') {
                return array (  '_controller' => 'Devschool\\BiblioBundle\\Controller\\DefaultController::listAction',  '_route' => 'page_livres',);
            }

            // page_livre
            if (preg_match('#^/livre/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'page_livre')), array (  '_controller' => 'Devschool\\BiblioBundle\\Controller\\DefaultController::showAction',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
